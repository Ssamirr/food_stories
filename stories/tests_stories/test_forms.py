from django.test import TestCase
from stories.forms import ContactForm


class ContactFormTestCase(TestCase):

    def setUp(self):
        self.valid_data = {
            'name': 'Samir',
            'email': 'sardarlisamir@gmail.com',
            'phone_number': '+99455174084',
            'subject': 'Sayt islemir',
            'message': 'https://stackoverflow.com/questions/7304248/how-should-i-write-tests-for-forms-in-django bu unvanda problem var',
        }
        self.invalid_data = {
            'name': 'SamirSaradrliSamirSaradrliSamirSaradrliSamirSaradrliSamirSaradrliSamirSaradrli',
            'email': 'sardarlisamir@gmail.com',
            'phone_number': '+994551740140184',
            'subject': 'Sayt islemir',
            'message': 'https://stackoverflow.com/questions/7304248/how-should-i-write-tests-for-forms-in-django bu unvanda problem var',
        }
        self.invalid_data2 = {
            'name': 'SamirSaradrliSamirSaradrliSamirSaradrliSamirSaradrliSamirSaradrliSamirSaradrli',
            'email': 'sardarlisamir@gmail.com',
            'phone_number': '+9945517401401845325236262224',
            'subject': 'Sayt islemir',
            'message': 'https://stackoverflow.com/questions/7304248/how-should-i-write-tests-for-forms-in-django bu unvanda problem var',
        }
        self.invalid_data3 = {
            'name': 'Samir',
            'email': 'sardarlisamir@gmail.com',
            'phone_number': '+994551740140',
        }

    def test_valid_data(self):
        form  = ContactForm(data=self.valid_data)
        self.assertTrue(form.is_valid())

    def test_invalid_data(self):
        form  = ContactForm(data=self.invalid_data)
        self.assertFalse(form.is_valid())
        self.assertTrue(form.errors)
        self.assertIn('name', form.errors.keys())
        self.assertIn('Bu dəyərin ən çox 40 simvol olduğuna əmin olun (78 var)', form.errors['name'])

    def test_invalid_data2(self):
        form  = ContactForm(data=self.invalid_data2)
        self.assertFalse(form.is_valid())
        self.assertTrue(form.errors)
        self.assertIn('phone_number', form.errors.keys())
        self.assertIn('Bu dəyərin ən çox 13 simvol olduğuna əmin olun (29 var)', form.errors['phone_number'])

    def test_invalid_data3(self):
        form  = ContactForm(data=self.invalid_data3)
        self.assertFalse(form.is_valid())
        self.assertTrue(form.errors)
        self.assertIn('subject', form.errors.keys())
        self.assertIn('message', form.errors.keys())